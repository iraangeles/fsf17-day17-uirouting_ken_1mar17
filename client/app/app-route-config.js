(function () {
    angular
        .module("uirouterApp")
        .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function uirouterAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl: "app/main/page1.html",
                controller : 'AppCtrl',
                controllerAs : 'ctrl'
            })
            .state("B", {
                url: "/B",
                templateUrl: "pages/page2.html",
                controller : 'AppCtrl',
                controllerAs : 'ctrl'
            })
            .state("C", {
                url: "/C",
                templateUrl: "pages/page3.html",
                controller : 'AppCtrl',
                controllerAs : 'ctrl'
            })
            .state("D", {
                url: "/D",
                templateUrl: "pages/people.html"
            })
            .state("E", {
                url: "/E/:personId",
                parent: 'D',
                templateUrl: "pages/person.html"
            })
            .state("F", {
                url: "/F",
                templateUrl: "pages/page4.html"
            })

        $urlRouterProvider.otherwise("/A");
    
    }

})();