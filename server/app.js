var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');


const NODE_PORT = process.env.NODE_PORT || 3000;

// TODO check the dirname is correct when test .
const CLIENT_FOLDER = path.join(__dirname , '../client');

var app = express();

// setup of the configuration of express.
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));

app.use(function(req,res){
    res.status(400).sendFile(path.join(CLIENT_FOLDER, "404.html"));
});

app.use(function(err, req, res, next){
    console.log("An error had occured 500");
    res.status(500).sendFile(path.join(CLIENT_FOLDER, "500.html"));
});


app.listen(NODE_PORT, function(){
    console.log("Server is running at port " + NODE_PORT);
})